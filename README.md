# README #

LEATHER (Library of Electronic and Active Thoughts Having Extraordinary Recipes) was a C# application written for my wife for our third anniversary (the traditional gift being leather).  She wanted to organize the recipes, so LEATHER was born.  I've since tried to make it less of a sandbox project and into something more FOSS-like.

### What is this repository for? ###

* LEATHER source code and released binary
* C# with Visual Studio 2015 Community
* 1.0.0.0
* [Recipes](https://bitbucket.org/beachedwhale42/recipes) in a separate repo

### How do I get set up? ###

* Clone, build
* No special configurations
* Visual Studio 2015 Community
* No special .exe installation, just run
* Requires Recipes/ directory in same directory as executable

### Contribution guidelines ###

* No official tests yet, feel free to add
* Code reviews go through me
* Spaces, no tabs :-)

### Who do I talk to? ###

* Me