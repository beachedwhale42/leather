﻿////////////////////////////////////////////////////////////////////////////////
//
// LEATHER 
// Library of Electronic and Active Thoughts Having Extraordinary Recipes
//
// This program is copyright David Sanders 2012
//
////////////////////////////////////////////////////////////////////////////////
// File: gui.cs

//
// System includes.
//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Leather
{
  public partial class gui : Form
  {
    ////////////////////////////////////////////////////////////////////////////
    //
    // Name: gui()
    //
    // Description: gui constructor.
    //
    // Notes:
    //
    ////////////////////////////////////////////////////////////////////////////
    public gui()
    {
      InitializeComponent();
    }

    ////////////////////////////////////////////////////////////////////////////
    //
    // Name: Form1_Load
    //
    // Description: When the form loads, start everything fresh.
    //
    // Notes:
    //
    ////////////////////////////////////////////////////////////////////////////
    private void Form1_Load(object sender, EventArgs e)
    {
      refreshListAndText();
    }

    ////////////////////////////////////////////////////////////////////////////
    //
    // Name: labelTitle_Click
    //
    // Description: Display version info upon clicking the label.
    //
    // Notes:
    //
    ////////////////////////////////////////////////////////////////////////////
    private void labelTitle_Click(object sender, EventArgs e)
    {
      //
      // Version info: Major.Minor.Revision.Release, Date
      //
      // Current Version: 1.0.0.0, May 29 2012
      //
      // Major:    Huge change (like going from Windows Forms to Metro UI)
      // Minor:    Added significant (but not major) feature(s) (like search)
      // Revision: Minor tweaks (like different text for new recipe)
      // Release:  Bug fixes (I shouldn't have to explain this)
      //
      MessageBox.Show("LEATHER  1.0.0.0\nMay 29, 2012", "About");
    }

    ////////////////////////////////////////////////////////////////////////////
    //
    // Name: recipeListBox_SelectedIndexChanged
    //
    // Description: Loads the contents of the selected file to a text box for
    //              reading and/or editing.
    //
    // Notes: Break this into a refreshTextBox function?
    //
    ////////////////////////////////////////////////////////////////////////////
    private void recipeListBox_SelectedIndexChanged(object sender, EventArgs e)
    {
      //
      // If there's an asterisk at the end of the filename (meaning a file is
      // selected and is currently being edited), pop up a dialog to verify that
      // the user really wants to cancel their changes and display the new file.
      // If the user clicks Yes, the text box will display the new file and mark
      // the text box as read only (basically just a "revert to last saved").
      // No asterisk, no file editing, thus nothing to cancel, and if the user
      // says No, they can continue editing as normal.
      //
      if (labelRecipeIngredients.Text.EndsWith("*", true, null))
      {
        if (MessageBox.Show("Are you sure you wish to switch recipes and lose current changes?",
                            "O RLY?",
                            MessageBoxButtons.YesNo) == DialogResult.Yes)
        {
          populateTextBox();
        }
      }
      else
      {
        populateTextBox();
      }
    }

    ////////////////////////////////////////////////////////////////////////////
    //
    // Name: buttonRefresh_Click
    //
    // Description: Clear the list box, clear and disable the text box, and
    //              re-populate the list box.
    //
    // Notes:
    //
    ////////////////////////////////////////////////////////////////////////////
    private void refreshListAndText()
    {
      //
      // Set up the labels and clear the label saying which recipe since none
      // is selected.
      //
      labelRecipeDir.Text = "List of Recipes";
      labelRecipeIngredients.Text = "";

      //
      // Clear the list box and text box, and mark the text box read only.
      //
      recipeListBox.Items.Clear();
      recipeTextBox.Clear();
      recipeTextBox.ReadOnly = true;

      //
      // Get the list of files.
      //
      if (Directory.Exists(m_theDir))
      {
        string[] files = Directory.GetFiles(m_theDir);

        //
        // Add filenames from the list to recipeListBox.
        //
        foreach (string i in files)
        {
          recipeListBox.Items.Add(i);
        }
      }
      else
      {
        MessageBox.Show("\"Recipes\\\" directory does not exist!");
      }
    }

    ////////////////////////////////////////////////////////////////////////////
    //
    // Name: populateTextBox
    //
    // Description: Populate the text box contents with the saved file.
    //
    // Notes:
    //
    ////////////////////////////////////////////////////////////////////////////
    private void populateTextBox()
    {
      //
      // If we're populating the text box with fresh, squeaky clean text,
      // mark it as read only.
      //
      recipeTextBox.ReadOnly = true;

      //
      // Set up the filename for the stream reader.  Set the label to that
      // filename.
      //
      string filename = recipeListBox.SelectedItem.ToString();
      string textline = "";
      labelRecipeIngredients.Text = filename;

      //
      // Only try reading the file if it exists.
      //
      if (System.IO.File.Exists(filename))
      {
        StreamReader reader = new StreamReader(filename);

        try
        {
          //
          // Read in the file, adding the newlines at the end of each line.
          // Then take the text we read in and print it to the text box.
          //
          do
          {
            textline += reader.ReadLine() + "\r\n";
          }
          while (reader.Peek() != -1);
          recipeTextBox.Text = textline;
        }

        catch
        {
          //
          // If we couldn't read in the file, make the text box blank, and let
          // the user know we couldn't read the file.
          //
          recipeTextBox.Text = "";
          MessageBox.Show("File is empty, corrupt, or not compatible.");
        }

        finally
        {
          //
          // Always execute this code, even if an error is caught.  Yay cleanup!
          //
          reader.Close();
        }
      }
      else
      {
        //
        // If the file does not exist, let the user know, and refresh the
        // list and text boxen.
        //
        MessageBox.Show("File does not exist.");
        refreshListAndText();
      }
    }

    ////////////////////////////////////////////////////////////////////////////
    //
    // Name: buttonRefresh_Click
    //
    // Description: Clear the list box, clear and disable the text box, and
    //              re-populate the list box.
    //
    // Notes:
    //
    ////////////////////////////////////////////////////////////////////////////
    private void buttonRefresh_Click(object sender, EventArgs e)
    {
      refreshListAndText();
    }

    ////////////////////////////////////////////////////////////////////////////
    //
    // Name: buttonEdit_Click
    //
    // Description: Enables the text box for editing.
    //
    // Notes:
    //
    ////////////////////////////////////////////////////////////////////////////
    private void buttonEdit_Click(object sender, EventArgs e)
    {
      //
      // Add a * to the end of the name to signify it's being edited.  The *
      // will only be added if there is a file selected and if there's not
      // already a * there.
      //
      if (labelRecipeIngredients.Text != "" &&
          !labelRecipeIngredients.Text.EndsWith("*", true, null))
      {
        recipeTextBox.ReadOnly = false;
        labelRecipeIngredients.Text = labelRecipeIngredients.Text + "*";
      }
      else
      {
        MessageBox.Show("Please select a file to edit, or create a new file.");
      }
    }

    ////////////////////////////////////////////////////////////////////////////
    //
    // Name: buttonCancel_Click
    //
    // Description: Disables the text box for editing, discards changes.
    //
    // Notes:
    //
    ////////////////////////////////////////////////////////////////////////////
    private void buttonCancel_Click(object sender, EventArgs e)
    {
      //
      // If there's an asterisk at the end of the filename (meaning a file is
      // selected and is currently being edited), pop up a dialog to verify that
      // the user really wants to cancel their changes.  If the user clicks Yes,
      // the text box will refresh with what is currently saved and mark the
      // text box as read only (basically just a "revert to last saved").
      // No asterisk, no file editing, thus nothing to cancel, and if the user
      // says No, they can continue editing as normal.
      //
      if (labelRecipeIngredients.Text.EndsWith("*", true, null))
      {
        if (MessageBox.Show("Are you sure you wish to cancel?",
                            "Cancel",
                            MessageBoxButtons.YesNo) ==
            DialogResult.Yes)
        {
          populateTextBox();
          recipeTextBox.ReadOnly = true;
        }
      }
    }

    ////////////////////////////////////////////////////////////////////////////
    //
    // Name: buttonSave_Click
    //
    // Description: Saves the contents of the text box to the file.
    //
    // Notes:
    //
    ////////////////////////////////////////////////////////////////////////////
    private void buttonSave_Click(object sender, EventArgs e)
    {
      //
      // This will always try to save the file that is currently being edited,
      // so only save if we're editing a file (thus there's a name in the label,
      // and it ends with a *).
      //
      if (labelRecipeIngredients.Text != "" && 
          labelRecipeIngredients.Text.EndsWith("*", true, null))
      {
        //
        // Set up filename and stream writer.
        //
        string filename = recipeListBox.SelectedItem.ToString();
        System.IO.StreamWriter writer = new System.IO.StreamWriter(filename);

        //
        // If the write is successful, let the user know, and disable the text
        // box for writing (in theory, the user is done editing for now), and
        // reset the name in the label.  If it failed, let the user know.
        // At the end, always close the write stream.
        //
        try
        {
          writer.Write(recipeTextBox.Text);
          MessageBox.Show(String.Format("{0} has been saved!", filename));
          recipeTextBox.ReadOnly = true;
          labelRecipeIngredients.Text = recipeListBox.SelectedItem.ToString();
        }
        catch
        {
          MessageBox.Show("Cannot write to file, aborting.");
        }
        finally
        {
          writer.Close();
        }
      }
    }

    ////////////////////////////////////////////////////////////////////////////
    //
    // Name: buttonCd_Click
    //
    // Description: Change recipe directory.
    //
    // Notes:
    //
    ////////////////////////////////////////////////////////////////////////////
    private void buttonCd_Click(object sender, EventArgs e)
    {
      //
      // If the user selects "OK" in the dialog window, cd to there.  The rest
      // of the form will update with it.
      //
      if (cdDialog.ShowDialog() == DialogResult.OK)
      {
        m_theDir = cdDialog.SelectedPath;
        refreshListAndText();
      }
    }

    ////////////////////////////////////////////////////////////////////////////
    //
    // Name: buttonNew_Click
    //
    // Description: Creates a new recipe in the current directory, saves it,
    //              and lets the user start editing it immediately.
    //
    // Notes:
    //
    ////////////////////////////////////////////////////////////////////////////
    private void buttonNew_Click(object sender, EventArgs e)
    {
      //
      // Set up dialog window defaults.
      //
      newDialog.InitialDirectory = m_theDir;
      newDialog.AddExtension     = true;
      newDialog.Title            = "New Recipe";
      newDialog.FileName         = "NewRecipe.txt";
      newDialog.DefaultExt       = ".txt";

      //
      // Variables for keeping track of things.
      //
      bool   successfulWrite = false;
      string filename        = "";

      //
      // Ask for filename.
      //
      if (newDialog.ShowDialog() == DialogResult.OK)
      {
        //
        // Set up filename and stream writer.
        //
        filename = newDialog.FileName;
        System.IO.StreamWriter writer = new System.IO.StreamWriter(filename);

        //
        // Put some starter text in the recipe.  If the write is successful,
        // set the success flag, and let the user know.  If it failed, let the
        // user know.  At the end, always close the write stream.
        //
        try
        {
          writer.Write("[Recipe Name]\n\nIngredients:\n\nDirections:\n");
          MessageBox.Show(String.Format("{0} has been created!", filename));
          successfulWrite = true;
        }
        catch
        {
          MessageBox.Show("Cannot write to file, aborting.");
        }
        finally
        {
          writer.Close();
        }

        //
        // If the write was successful, refresh the list and text box.  Find
        // the entry in the list box that has the filename, auto-select it,
        // and "click" the edit button.
        //
        if (successfulWrite)
        {
          refreshListAndText();
          // FIXME auto selecting the file will only work if in the same dir.
          // Need to cd to filedir before selecting it.
          recipeListBox.SelectedIndex = recipeListBox.FindString(filename);
          buttonEdit_Click(sender, e);
        }
      }
    }

    ////////////////////////////////////////////////////////////////////////////
    //
    // Name: buttonPrint_Click
    //
    // Description: Opens the print dialog window to print the recipe.
    //
    // Notes:
    //
    ////////////////////////////////////////////////////////////////////////////
    private void buttonPrint_Click(object sender, EventArgs e)
    {
      //
      // Create the print document object and prepare it for printing.
      //
      System.Drawing.Printing.PrintDocument printDoc =
        new System.Drawing.Printing.PrintDocument();

      //
      // If there's no filename, or if it's not saved, tell the user to save
      // the recipe first.
      //
      if (labelRecipeIngredients.Text == "" ||
          labelRecipeIngredients.Text.EndsWith("*", true, null))
      {
        MessageBox.Show("Please save file before printing.");
      }
      else
      {
        printDoc.DocumentName      = labelRecipeIngredients.Text;
        printDialog.Document       = printDoc;
        printDialog.AllowSelection = true;
        printDialog.AllowSomePages = true;
        printDialog.ShowHelp       = true;

        //
        // If the user selects "OK", print.
        //
        if (printDialog.ShowDialog() == DialogResult.OK)
        {
          m_strToPrint = recipeTextBox.Text;

          //
          // Magic.  See printDoc_PrintPage().
          //
          printDoc.PrintPage += new
            System.Drawing.Printing.PrintPageEventHandler
              (printDoc_PrintPage);

          printDoc.Print();
        }
      }
    }

    ////////////////////////////////////////////////////////////////////////////
    //
    // Name: printDoc_PrintPage
    //
    // Description: Magic I found to print stuff.
    //
    // Notes: http://social.msdn.microsoft.com/forums/en-US/Vsexpressvcs/thread/6fb71753-8ac5-44c5-80fa-733b7258d2ca
    //
    ////////////////////////////////////////////////////////////////////////////
    void printDoc_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
    {
      int linesPerPage = 0;
      int charsOnPage  = 0;

      e.Graphics.MeasureString(m_strToPrint,
                               recipeTextBox.Font,
                               e.MarginBounds.Size,
                               StringFormat.GenericDefault,
                               out charsOnPage,
                               out linesPerPage);

      e.Graphics.DrawString(m_strToPrint,
                            recipeTextBox.Font,
                            Brushes.Black,
                            e.MarginBounds,
                            StringFormat.GenericDefault);

      m_strToPrint = m_strToPrint.Substring(charsOnPage);

      e.HasMorePages = (m_strToPrint.Length > 0);

    }

    /*
    ////////////////////////////////////////////////////////////////////////////
    //
    // Name: buttonSearch_Click
    //
    // Description: Launch the search feature
    //
    // Notes:
    //
    ////////////////////////////////////////////////////////////////////////////
    private void buttonSearch_Click(object sender, EventArgs e)
    {
      //
      // Ask for search string.
      //
      //ask()

      //
      // Call grep.
      //
      bool result = ProcessGrep("asdf");

      //
      // If we found it, let the user know where.  Otherwise, tell them
      // that nothing is there.
      //
      if (result)
      {
      }
      else
      {
        MessageBox.Show("No recipes were found containing that.");
      }
    }

    ////////////////////////////////////////////////////////////////////////////
    //
    // Name: ProcessGrep
    //
    // Description: Process the actual search using grep
    //
    // Notes:
    //
    ////////////////////////////////////////////////////////////////////////////
    public bool ProcessGrep(string searchString)
    {
      string grepArguments = "-Hil";

      System.Diagnostics.Process GrepProcess = new
        System.Diagnostics.Process();
      System.Diagnostics.ProcessStartInfo GrepStartInfo =
        new System.Diagnostics.ProcessStartInfo();

      GrepStartInfo.FileName = "CMD.exe ";

      GrepStartInfo.RedirectStandardError = false;
      GrepStartInfo.RedirectStandardOutput = false;
      GrepStartInfo.RedirectStandardInput = false;
      GrepStartInfo.UseShellExecute = false;
      GrepStartInfo.CreateNoWindow = true;

      //
      // Call grep with the arguments, string, search in *.txt, and output
      // the results to a text file for us to read.
      //
      GrepStartInfo.Arguments = "/D /c grep " + grepArguments + " " +
                                searchString + " *.txt > grep.txt";

      GrepProcess.EnableRaisingEvents = true;
      GrepProcess.StartInfo = GrepStartInfo;

      //start cmd.exe & the grep process
      GrepProcess.Start();

      //set the wait period for exiting the process
      GrepProcess.WaitForExit(15000); //or the wait time you want

      int ExitCode = GrepProcess.ExitCode;
      bool GrepSuccessful = true;

      //Now we need to see if the process was successful
      if (ExitCode > 0 & !GrepProcess.HasExited)
      {
        GrepProcess.Kill();
        GrepSuccessful = false;
      }

      //now clean up after ourselves
      GrepProcess.Dispose();
      GrepStartInfo = null;
      return GrepSuccessful;
    }
    */

    ////////////////////////////////////////////////////////////////////////////
    //
    // Private Member Variables
    //
    ////////////////////////////////////////////////////////////////////////////
    private string m_theDir = System.Environment.CurrentDirectory + "\\Recipes";
    private string m_strToPrint = String.Empty;

  }

}

