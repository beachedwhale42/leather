﻿////////////////////////////////////////////////////////////////////////////////
//
// LEATHER 
// Library of Electronic and Active Thoughts Having Extraordinary Recipes
//
// This program is copyright David Sanders 2012
//
////////////////////////////////////////////////////////////////////////////////
// File: Leather.cs

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Leather
{
  static class Leather
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main()
    {
      //
      // This is where the main application is kicked off.
      //
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Application.Run(new gui());
    }
  }
}
