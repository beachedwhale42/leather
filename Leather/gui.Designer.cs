﻿namespace Leather
{
  partial class gui
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(gui));
      this.recipeListBox = new System.Windows.Forms.ListBox();
      this.recipeTextBox = new System.Windows.Forms.TextBox();
      this.buttonSave = new System.Windows.Forms.Button();
      this.buttonRefresh = new System.Windows.Forms.Button();
      this.labelRecipeDir = new System.Windows.Forms.Label();
      this.buttonEdit = new System.Windows.Forms.Button();
      this.buttonCancel = new System.Windows.Forms.Button();
      this.labelTitle = new System.Windows.Forms.Label();
      this.labelRecipeIngredients = new System.Windows.Forms.Label();
      this.buttonCd = new System.Windows.Forms.Button();
      this.buttonNew = new System.Windows.Forms.Button();
      this.cdDialog = new System.Windows.Forms.FolderBrowserDialog();
      this.newDialog = new System.Windows.Forms.SaveFileDialog();
      this.buttonPrint = new System.Windows.Forms.Button();
      this.printDialog = new System.Windows.Forms.PrintDialog();
      this.SuspendLayout();
      // 
      // recipeListBox
      // 
      this.recipeListBox.BackColor = System.Drawing.Color.DarkGray;
      this.recipeListBox.FormattingEnabled = true;
      this.recipeListBox.HorizontalScrollbar = true;
      this.recipeListBox.Location = new System.Drawing.Point(93, 50);
      this.recipeListBox.Name = "recipeListBox";
      this.recipeListBox.Size = new System.Drawing.Size(325, 433);
      this.recipeListBox.TabIndex = 0;
      this.recipeListBox.SelectedIndexChanged += new System.EventHandler(this.recipeListBox_SelectedIndexChanged);
      // 
      // recipeTextBox
      // 
      this.recipeTextBox.AllowDrop = true;
      this.recipeTextBox.BackColor = System.Drawing.Color.DarkGray;
      this.recipeTextBox.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.recipeTextBox.Location = new System.Drawing.Point(470, 50);
      this.recipeTextBox.Multiline = true;
      this.recipeTextBox.Name = "recipeTextBox";
      this.recipeTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
      this.recipeTextBox.Size = new System.Drawing.Size(445, 435);
      this.recipeTextBox.TabIndex = 2;
      // 
      // buttonSave
      // 
      this.buttonSave.Location = new System.Drawing.Point(921, 205);
      this.buttonSave.Name = "buttonSave";
      this.buttonSave.Size = new System.Drawing.Size(75, 23);
      this.buttonSave.TabIndex = 3;
      this.buttonSave.Text = "Save";
      this.buttonSave.UseVisualStyleBackColor = true;
      this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
      // 
      // buttonRefresh
      // 
      this.buttonRefresh.Location = new System.Drawing.Point(12, 85);
      this.buttonRefresh.Name = "buttonRefresh";
      this.buttonRefresh.Size = new System.Drawing.Size(75, 23);
      this.buttonRefresh.TabIndex = 5;
      this.buttonRefresh.Text = "Refresh";
      this.buttonRefresh.UseVisualStyleBackColor = true;
      this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
      // 
      // labelRecipeDir
      // 
      this.labelRecipeDir.AutoSize = true;
      this.labelRecipeDir.BackColor = System.Drawing.Color.Transparent;
      this.labelRecipeDir.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelRecipeDir.ForeColor = System.Drawing.SystemColors.ActiveCaption;
      this.labelRecipeDir.Location = new System.Drawing.Point(89, 18);
      this.labelRecipeDir.Name = "labelRecipeDir";
      this.labelRecipeDir.Size = new System.Drawing.Size(119, 23);
      this.labelRecipeDir.TabIndex = 6;
      this.labelRecipeDir.Text = "labelRecipeDir";
      // 
      // buttonEdit
      // 
      this.buttonEdit.Location = new System.Drawing.Point(921, 125);
      this.buttonEdit.Name = "buttonEdit";
      this.buttonEdit.Size = new System.Drawing.Size(75, 23);
      this.buttonEdit.TabIndex = 7;
      this.buttonEdit.Text = "Edit";
      this.buttonEdit.UseVisualStyleBackColor = true;
      this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
      // 
      // buttonCancel
      // 
      this.buttonCancel.Location = new System.Drawing.Point(921, 165);
      this.buttonCancel.Name = "buttonCancel";
      this.buttonCancel.Size = new System.Drawing.Size(75, 23);
      this.buttonCancel.TabIndex = 8;
      this.buttonCancel.Text = "Cancel";
      this.buttonCancel.UseVisualStyleBackColor = true;
      this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
      // 
      // labelTitle
      // 
      this.labelTitle.AutoSize = true;
      this.labelTitle.BackColor = System.Drawing.Color.Transparent;
      this.labelTitle.Font = new System.Drawing.Font("Harlow Solid Italic", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelTitle.ForeColor = System.Drawing.Color.DarkGray;
      this.labelTitle.Location = new System.Drawing.Point(26, 517);
      this.labelTitle.Name = "labelTitle";
      this.labelTitle.Size = new System.Drawing.Size(958, 36);
      this.labelTitle.TabIndex = 9;
      this.labelTitle.Text = "Library of Electronic and Active Thoughts Having Extraordinary Recipes";
      this.labelTitle.Click += new System.EventHandler(this.labelTitle_Click);
      // 
      // labelRecipeIngredients
      // 
      this.labelRecipeIngredients.AutoSize = true;
      this.labelRecipeIngredients.BackColor = System.Drawing.Color.Transparent;
      this.labelRecipeIngredients.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelRecipeIngredients.ForeColor = System.Drawing.SystemColors.ActiveCaption;
      this.labelRecipeIngredients.Location = new System.Drawing.Point(466, 18);
      this.labelRecipeIngredients.Name = "labelRecipeIngredients";
      this.labelRecipeIngredients.Size = new System.Drawing.Size(184, 23);
      this.labelRecipeIngredients.TabIndex = 10;
      this.labelRecipeIngredients.Text = "labelRecipeIngredients";
      // 
      // buttonCd
      // 
      this.buttonCd.Location = new System.Drawing.Point(12, 125);
      this.buttonCd.Name = "buttonCd";
      this.buttonCd.Size = new System.Drawing.Size(75, 46);
      this.buttonCd.TabIndex = 11;
      this.buttonCd.Text = "Change Directory";
      this.buttonCd.UseVisualStyleBackColor = true;
      this.buttonCd.Click += new System.EventHandler(this.buttonCd_Click);
      // 
      // buttonNew
      // 
      this.buttonNew.Location = new System.Drawing.Point(921, 85);
      this.buttonNew.Name = "buttonNew";
      this.buttonNew.Size = new System.Drawing.Size(75, 23);
      this.buttonNew.TabIndex = 12;
      this.buttonNew.Text = "New";
      this.buttonNew.UseVisualStyleBackColor = true;
      this.buttonNew.Click += new System.EventHandler(this.buttonNew_Click);
      // 
      // buttonPrint
      // 
      this.buttonPrint.Location = new System.Drawing.Point(921, 245);
      this.buttonPrint.Name = "buttonPrint";
      this.buttonPrint.Size = new System.Drawing.Size(75, 23);
      this.buttonPrint.TabIndex = 13;
      this.buttonPrint.Text = "Print";
      this.buttonPrint.UseVisualStyleBackColor = true;
      this.buttonPrint.Click += new System.EventHandler(this.buttonPrint_Click);
      // 
      // printDialog
      // 
      this.printDialog.UseEXDialog = true;
      // 
      // gui
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
      this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.ClientSize = new System.Drawing.Size(1008, 562);
      this.Controls.Add(this.buttonPrint);
      this.Controls.Add(this.buttonNew);
      this.Controls.Add(this.buttonCd);
      this.Controls.Add(this.labelRecipeIngredients);
      this.Controls.Add(this.labelTitle);
      this.Controls.Add(this.buttonCancel);
      this.Controls.Add(this.buttonEdit);
      this.Controls.Add(this.labelRecipeDir);
      this.Controls.Add(this.buttonRefresh);
      this.Controls.Add(this.buttonSave);
      this.Controls.Add(this.recipeTextBox);
      this.Controls.Add(this.recipeListBox);
      this.DoubleBuffered = true;
      this.MaximizeBox = false;
      this.Name = "gui";
      this.ShowIcon = false;
      this.Text = "LEATHER";
      this.Load += new System.EventHandler(this.Form1_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ListBox recipeListBox;
    private System.Windows.Forms.TextBox recipeTextBox;
    private System.Windows.Forms.Button buttonSave;
    private System.Windows.Forms.Button buttonRefresh;
    private System.Windows.Forms.Label labelRecipeDir;
    private System.Windows.Forms.Button buttonEdit;
    private System.Windows.Forms.Button buttonCancel;
    private System.Windows.Forms.Label labelTitle;
    private System.Windows.Forms.Label labelRecipeIngredients;
    private System.Windows.Forms.Button buttonCd;
    private System.Windows.Forms.Button buttonNew;
    private System.Windows.Forms.FolderBrowserDialog cdDialog;
    private System.Windows.Forms.SaveFileDialog newDialog;
    private System.Windows.Forms.Button buttonPrint;
    private System.Windows.Forms.PrintDialog printDialog;
  }
}

